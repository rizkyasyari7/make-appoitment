import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

RunConfiguration.setWebDriverPreferencesProperty('args', ['window-size=1920,1080'])

def path = RunConfiguration.getProjectDir().replace('/', '\\')

WebUI.openBrowser(GlobalVariable.URL)

WebUI.deleteAllCookies()

WebUI.click(findTestObject('Object Repository/Login/Make Appointment'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Login/Make Appointment'), 2)

'Open URL'
WebUI.takeScreenshot()

WebUI.sendKeys(findTestObject('Object Repository/Login/Input Username'), GlobalVariable.username_positive)

'Input Username'
WebUI.takeScreenshot()

WebUI.sendKeys(findTestObject('Object Repository/Login/Input Password'), GlobalVariable.password_positive)

'Input Password'
WebUI.takeScreenshot()

WebUI.waitForElementClickable(findTestObject('Object Repository/Login/Input Password'), 2)

WebUI.click(findTestObject('Object Repository/Login/Button Login'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Login/Button Login'), 2)

'Sucess Login'
WebUI.takeScreenshot()

