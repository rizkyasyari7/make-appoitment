import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable

import org.apache.commons.lang.RandomStringUtils
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import com.kms.katalon.core.configuration.RunConfiguration


RunConfiguration.setWebDriverPreferencesProperty('args', ['window-size=1920,1080'])

def path = RunConfiguration.getProjectDir().replace('/', '\\')


'Klik Facility'
WebUI.selectOptionByIndex(findTestObject('Object Repository/Make Appoitment/Facility'), 2 )

WebUI.delay(2)

WebUI.takeScreenshot()

'Klik Checkbox'
WebUI.click(findTestObject('Object Repository/Make Appoitment/input Apply for hospital readmission'))

WebUI.delay(2)

WebUI.takeScreenshot()

'Klik Button'
WebUI.click(findTestObject('Object Repository/Make Appoitment/input Medicare programs'))

WebUI.delay(2)

WebUI.takeScreenshot()

'Klik Visit Date'
WebUI.click(findTestObject('Object Repository/Make Appoitment/Visit Date'))

WebUI.delay(2)

WebUI.takeScreenshot()

Date nowdate = new Date()
def date = nowdate.plus(1).format("d");

def month = nowdate.plus(1).format("MMM");

def year = nowdate.plus(1).format("yyyy");

def Switch1 = nowdate.format("MMMM yyyy");

WebUI.click(findTestObject('Object Repository/Make Appoitment/Switch Date 1' , ['switch' : Switch1]))

WebUI.click(findTestObject('Object Repository/Make Appoitment/Switch Date 2' , ['switch2' : year]))

WebUI.click(findTestObject('Object Repository/Make Appoitment/Pick Years' , ['tahun' : year]))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Make Appoitment/Pick Month' , ['bulan' : month]))

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Make Appoitment/Pick Date' , ['hari' : date]))

WebUI.delay(3)

WebUI.takeScreenshot()

'Input Komen'
String randomnumeric = RandomStringUtils.randomNumeric(4)

WebUI.setText(findTestObject('Object Repository/Make Appoitment/Comment'), 'Ini Adalah Komentar ke ' + randomnumeric)

WebUI.waitForElementClickable(findTestObject('Object Repository/Make Appoitment/Comment'), 2)

WebUI.takeScreenshot()

'Button BookApp'
WebUI.click(findTestObject('Object Repository/Make Appoitment/button Book Appointment'))

WebUI.delay(3)

WebUI.takeScreenshot()


'Dashboard'
WebUI.click(findTestObject('Object Repository/Make Appoitment/Go to Homepage'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Make Appoitment/Go to Homepage'), 2)

WebUI.takeScreenshot()

