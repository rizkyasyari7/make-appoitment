<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Switch Date 2</name>
   <tag></tag>
   <elementGuidId>9f1a8533-b629-4364-a132-7d94ec0fcfde</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot;datepicker-switch&quot;  and (text() = &quot;${switch2}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot;datepicker-switch&quot;  and (text() = &quot;${switch2}&quot;)]</value>
      <webElementGuid>b9c37d68-5cbb-4a52-9d50-aad5955bff94</webElementGuid>
   </webElementProperties>
</WebElementEntity>
