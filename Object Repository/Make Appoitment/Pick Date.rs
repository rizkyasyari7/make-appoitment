<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pick Date</name>
   <tag></tag>
   <elementGuidId>4c480273-bf49-41cb-b901-3245870a19f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'day' and (text() = '${hari}' or . = '${hari}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>day</value>
      <webElementGuid>cdb8a411-b7bf-4a47-b148-6467eacfb86e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>${hari}</value>
      <webElementGuid>f01314f1-7759-44d1-a74b-e71fc564a295</webElementGuid>
   </webElementProperties>
</WebElementEntity>
