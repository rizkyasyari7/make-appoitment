<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Switch Date 1</name>
   <tag></tag>
   <elementGuidId>45dd0ca2-e8c7-4d17-b5d8-ad07c78c3da1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot;datepicker-switch&quot;  and (text() = &quot;${switch}&quot;)]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;datepicker-switch&quot;  and (text() = &quot;July 2023&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot;datepicker-switch&quot;  and (text() = &quot;${switch}&quot;)]</value>
      <webElementGuid>80110f17-803d-4561-8b0c-542141214e3b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
