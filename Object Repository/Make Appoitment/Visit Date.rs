<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Visit Date</name>
   <tag></tag>
   <elementGuidId>676f335b-02c5-4357-a29c-1e56f97dbef0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.input-group-addon</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'glyphicon glyphicon-calendar']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='appointment']/div/div/form/div[4]/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7352ccb3-0f3d-41eb-b2dd-0524fa2d75f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>glyphicon glyphicon-calendar</value>
      <webElementGuid>72747200-5092-4a31-893a-e5d5e33c350f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/form[@class=&quot;form-horizontal&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;col-sm-4&quot;]/div[@class=&quot;input-group date&quot;]/div[@class=&quot;input-group-addon&quot;]</value>
      <webElementGuid>52154499-cafe-4459-b6e4-c4bc3fe4daba</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[4]/div/div/div</value>
      <webElementGuid>ebb60490-5db2-4186-a60d-4b24efaa5291</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Date (Required)'])[1]/following::div[3]</value>
      <webElementGuid>6f5eb0f3-7524-4b96-a6ac-c2a6af1b9d42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comment'])[1]/preceding::div[1]</value>
      <webElementGuid>80cbf9d0-a612-450c-a4c3-52aa5e6fc161</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Book Appointment'])[1]/preceding::div[3]</value>
      <webElementGuid>f8c9e7f6-45c4-4812-974c-c6b5d3acf4d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div</value>
      <webElementGuid>3a380d6f-fbc2-4058-a985-72d77dd5cb44</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
