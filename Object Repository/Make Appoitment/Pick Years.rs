<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pick Years</name>
   <tag></tag>
   <elementGuidId>3a068209-2f84-4238-a72d-8514114c83df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'year') and (text() = '${tahun}' or . = '${tahun}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>year</value>
      <webElementGuid>2b8f1bc3-4694-4731-ad7b-df52126e87f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>${tahun}</value>
      <webElementGuid>ade4be5a-9e51-4539-ba87-338f4142c049</webElementGuid>
   </webElementProperties>
</WebElementEntity>
