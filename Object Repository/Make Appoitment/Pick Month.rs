<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pick Month</name>
   <tag></tag>
   <elementGuidId>7a3352b9-97f0-4e61-8357-4fe2660d7f0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'month') and (text() = '${bulan}' or . = '${bulan}')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>month</value>
      <webElementGuid>ba8fc4c5-d701-4528-acd5-4d0de5bde5f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>${bulan}</value>
      <webElementGuid>79afc709-e953-4b56-b859-cc5cbb6fea61</webElementGuid>
   </webElementProperties>
</WebElementEntity>
